package jp.co.tis.stc.webtalkie;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.net.Proxy;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

class TextToVoiceTask extends AsyncTask<Object, Integer, CompletedHttpResponse> {

	private URI uri;
	private OnConvertCompleteListener callbackHandler;
	
	public TextToVoiceTask(URI uri, OnConvertCompleteListener callbackHandler) {
		this.uri = uri;
		this.callbackHandler = callbackHandler;
	}

	@Override
	@SuppressWarnings("unchecked")
	protected CompletedHttpResponse doInBackground(Object... params) {
		AndroidHttpClient httpClient = AndroidHttpClient.newInstance("android");
		String proxyHost = Proxy.getDefaultHost();
		int proxyPort = Proxy.getDefaultPort();

		if (proxyHost != null && proxyPort > 0) {
			HttpHost proxy = new HttpHost(proxyHost, proxyPort);
			ConnRouteParams.setDefaultProxy(httpClient.getParams(), proxy);
		}
		HttpPost httpPost = new HttpPost(uri.toString());

		final List<NameValuePair> requestParams = new ArrayList<NameValuePair>();
		HashMap<String, Object> parameters = (HashMap<String, Object>)params[0];
		for(String key : parameters.keySet()) {
			requestParams.add(new BasicNameValuePair(key, parameters.get(key).toString()));
		}

		CompletedHttpResponse response = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(requestParams, HTTP.UTF_8));
			response = httpClient.execute(httpPost, new ResponseHandler<CompletedHttpResponse>() {
				@Override
				public CompletedHttpResponse handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
					if (response == null) throw new RuntimeException("Response is null.");

					return new CompletedHttpResponse(response);
				}
			});
		} catch (Exception e) {
			response = new CompletedHttpResponse(e);
		} finally {
			httpClient.getConnectionManager().shutdown();
			httpClient.close();
		}

		return response;
	}

	@Override
	protected void onPostExecute(CompletedHttpResponse response) {
		if(response != null) {
			if(response.getStatusCode() == HttpStatus.SC_OK) {
				callbackHandler.onConvertComplete(response.getBodyStream());
			} else {
				callbackHandler.onConvertError(response.getStatusCode(), response.getBodyText());
			}
		} else {
			callbackHandler.onConvertError(-1, "{\"error\": \"Unknown: Response is null.\"}");
		}
	}
}
