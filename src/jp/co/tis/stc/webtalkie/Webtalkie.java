package jp.co.tis.stc.webtalkie;

import java.net.URI;
import java.util.HashMap;

/**
 * 音声合成サービスWebtalkieをAndroidから使うためのクラスです。
 */
public class Webtalkie {
	private String apiKey;
	private URI uri;
	
	private TextToVoiceTask textToVoiceTask;
	
	private String speakerName = "nozomi_22";
	private float volume = 1.0f;
	private float speed = 1.0f;
	private float pitch = 1.0f;
	private float range = 1.0f;
	private int pauseMiddle = 150;
	private int pauseLong = 370;
	private int pauseSentence = 800;
	
	/**
	 * クラスを初期化しAPIを呼び出す準備を整えます。
	 * @param uri 音声合成APIのURL
	 * @param apiKey 各ユーザに割り当てられたAPIキー(音声合成Proxyを経由する場合にはnull)
	 */
	public Webtalkie(URI uri, String apiKey) {
		this.uri = uri;
		this.apiKey = apiKey;
	}

	/**
	 * 関連リソースを解放します。
	 */
	public void release()
	{
		if(textToVoiceTask != null) textToVoiceTask.cancel(true);
	}
	
	/**
	 * 音声合成を行います。
	 * @param text 読み上げる文字列
	 * @param callback 音声合成終了時に呼び出されるコールバックメソッド(onCallback)を実装したクラスのインスタンス
	 */
	public void textToVoice(String text, OnConvertCompleteListener callback)
	{
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("api_key", apiKey);
		parameters.put("text", text);

		parameters.put("speaker_name", speakerName);
		parameters.put("volume", volume);
		parameters.put("speed", speed);
		parameters.put("pitch", pitch);
		parameters.put("range", range);
		parameters.put("pause_middle", pauseMiddle);
		parameters.put("pause_long", pauseLong);
		parameters.put("pause_sentence", pauseSentence);
		
		textToVoiceTask = new TextToVoiceTask(uri, callback);
		textToVoiceTask.execute(parameters);
	}

	/**
	 * 生成された音声ファイルの形式を取得します。
	 * @return wav, mp3, oggのいずれかを返します
	 */
	public String getFormat() {
		String[] parts = this.uri.getPath().split("\\.");
		return parts[parts.length - 1];
	}

	/**
	 * 音声合成時に使用する話者を取得します。
	 * @return 話者コード(e.g. nozomi_22)
	 */
	public String getSpeakerName() {
		return speakerName;
	}

	/**
	 * 音声合成時に使用する話者を指定します。
	 * @param speakerName 話者コード(e.g. nozomi_22)
	 */
	public void setSpeakerName(String speakerName) {
		this.speakerName = speakerName;
	}

	/**
	 * 音声合成時の音量を取得します。
	 * @return 音量(0.0 ～ 2.0)
	 */
	public float getVolume() {
		return volume;
	}

	/**
	 * 音声合成時の音量を指定します。
	 * @param volume 音量(0.0 ～ 2.0)
	 */
	public void setVolume(float volume) {
		this.volume = volume;
	}

	/**
	 * 音声合成時の読み上げ速度を取得します。
	 * @return 読み上げ速度(0.5 ～ 4.0)
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * 音声合成時の読み上げ速度を指定します。
	 * @param speed 読み上げ速度(0.5 ～ 4.0)
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}

	/**
	 * 音声合成時の音の高さを取得します。
	 * @return 音の高さ(0.5 ～ 2.0)
	 */
	public float getPitch() {
		return pitch;
	}

	/**
	 * 音声合成時の音の高さを指定します。
	 * @param pitch 音の高さ(0.5 ～ 2.0)
	 */
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	/**
	 * 音声合成時の抑揚の強さを取得します。
	 * @return 抑揚の強さ(0.0 ～ 2.0)
	 */
	public float getRange() {
		return range;
	}

	/**
	 * 音声合成時の抑揚の強さを指定します。
	 * @param range 抑揚の強さ(0.0 ～ 2.0)
	 */
	public void setRange(float range) {
		this.range = range;
	}

	/**
	 * 音声合成時の短ポーズ長を取得します。
	 * @return 短ポーズ長(80ms ～ 300ms)
	 */
	public int getPauseMiddle() {
		return pauseMiddle;
	}

	/**
	 * 音声合成時の短ポーズ長を指定します。
	 * @param pauseMiddle 短ポーズ長(80ms ～ 300ms) ※長ポーズ長を超えることはできません
	 */
	public void setPauseMiddle(int pauseMiddle) {
		this.pauseMiddle = pauseMiddle;
	}

	/**
	 * 音声合成時の長ポーズ長を取得します。
	 * @return 長ポーズ長(100ms ～ 2000ms)
	 */
	public int getPauseLong() {
		return pauseLong;
	}

	/**
	 * 音声合成時の長ポーズ長を指定します。
	 * @param pauseLong 長ポーズ長(100ms ～ 2000ms) ※文末ポーズ長を超えることはできません
	 */
	public void setPauseLong(int pauseLong) {
		this.pauseLong = pauseLong;
	}

	/**
	 * 音声合成時の文末ポーズ長を取得します。
	 * @return 文末ポーズ長(300ms ～ 10000ms)
	 */
	public int getPauseSentence() {
		return pauseSentence;
	}

	/**
	 * 音声合成時の文末ポーズ長を指定します。
	 * @param pauseSentence 文末ポーズ長(300ms ～ 10000ms)
	 */
	public void setPauseSentence(int pauseSentence) {
		this.pauseSentence = pauseSentence;
	}
}
