package jp.co.tis.stc.webtalkie;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.CharArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

public final class CompletedHttpResponse {
	private int statusCode;
	private ByteArrayBuffer buffer;

	public CompletedHttpResponse(Exception e)
	{
		String json = null;
		try {
			JSONObject errorJSON = new JSONObject();
			errorJSON.put("error", e.toString());
			json = errorJSON.toString();
		} catch(JSONException ex) {
			json = e.toString();
		}

		statusCode = -1;
		CharArrayBuffer charArrayBuffer = new CharArrayBuffer(256);
		charArrayBuffer.append(json);
		buffer = new ByteArrayBuffer(256);
		buffer.append(charArrayBuffer, 0, charArrayBuffer.length());
	}

	public CompletedHttpResponse(HttpResponse response)
	{
		statusCode = response.getStatusLine().getStatusCode();
		buffer = contentToBuffer(response);
	}

	private ByteArrayBuffer contentToBuffer(HttpResponse response) {
		ByteArrayBuffer temporaryBuffer = new ByteArrayBuffer(16384);
		final int BUFFER_SIZE = 10240;
		
		try {
			InputStream is = response.getEntity().getContent();
			BufferedInputStream in = new BufferedInputStream(is, BUFFER_SIZE);

			byte buf[] = new byte[BUFFER_SIZE];
			int size = -1;
			while ((size = in.read(buf)) != -1) {
				temporaryBuffer.append(buf, 0, size);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return temporaryBuffer;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public ByteArrayBuffer getBodyBuffer() {
		return buffer;
	}
	
	public InputStream getBodyStream() {
		return new ByteArrayInputStream(buffer.toByteArray());
	}
	
	public String getBodyText() {
		try {
			return new String(buffer.toByteArray(), "UTF-8");
		} catch(UnsupportedEncodingException e) {
			return null;
		}
	}

}
