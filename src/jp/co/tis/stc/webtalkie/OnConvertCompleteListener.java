package jp.co.tis.stc.webtalkie;

import java.io.InputStream;

/**
 * 音声合成の終了時に呼び出されるコールバックメソッドを定義したインターフェイスです。 
 */
public interface OnConvertCompleteListener {
	/**
	 * 音声合成の終了時にWebtalkieライブラリから呼び出されます。
	 * @param binaryData 変換された音声ファイル(Wav / MP3 / Ogg)のバイナリデータです。
	 */
	public void onConvertComplete(InputStream binaryData);

	/**
	 * 音声合成の失敗時にWebtalkieライブラリから呼び出されます。
	 * @param statusCode HTTPステータスコードです。
	 * @param error エラー内容を示すJSON文字列です。
	 */
	public void onConvertError(int statusCode, String jsonError);
}
